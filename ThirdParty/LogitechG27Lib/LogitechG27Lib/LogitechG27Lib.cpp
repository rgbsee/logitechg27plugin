
#include "LogitechG27Lib.h"

#include <stdexcept>

#include "LogiWheel.h"
#include "LogiControllerInput.h"

using namespace std;
using namespace LogitechSteeringWheel;
using namespace LogitechControllerInput;

static Wheel* _wheel;
static ControllerInput* _controllerInput;
static bool initialised = false;

namespace LogitechG27Lib
{
	G27Lib::G27Lib()
	{
		// Initialize();
	}

	void G27Lib::Initialize()
	{
		// only do this once, if we already have wheel just reinit.
		if (!initialised)
		{
			_controllerInput = new ControllerInput(GetForegroundWindow(), TRUE); // TRUE means that XInput devices will be ignored
			_wheel = new Wheel(_controllerInput);
		}
		SetProperties(true, 100, 100, 100, false, 900, true);
		initialised = true;
	}

	void G27Lib::SetProperties(bool enableForce, int overallGain, int springGain, int damperGain, bool combinePedals, int wheelRange, bool enableDefaultSpring)
	{
		if (_wheel != NULL)
		{
			ControllerPropertiesData propertiesData;
			ZeroMemory(&propertiesData, sizeof(propertiesData));
			propertiesData.forceEnable = enableForce;
			propertiesData.overallGain = overallGain;
			propertiesData.springGain = springGain;
			propertiesData.damperGain = damperGain;
			propertiesData.combinePedals = combinePedals;
			propertiesData.wheelRange = wheelRange;
			propertiesData.defaultSpringEnabled = enableDefaultSpring;
			propertiesData.allowGameSettings = true;

			// We simply set our preferred setting for the game. The Steering Wheel SDK will take care of attempting to set it
			// whenever necessary.
			_wheel->SetPreferredControllerProperties(propertiesData);			
		}
	}

	void G27Lib::Deactivate()
	{
		if (_wheel)
		{
			delete _wheel;
			_wheel = NULL;
		}

		if (_controllerInput)
		{
			delete _controllerInput;
			_controllerInput = NULL;
		}

		initialised = false;

	}

	int G27Lib::IsConnected()
	{
		//Wheel is not connected before we have the plugin
		if (_wheel == NULL || !_wheel) //(!initialised || !_wheel || _wheel == NULL)
			return -1;
		if (!_wheel->IsConnected(0, DeviceType::LG_DEVICE_TYPE_WHEEL) && !_wheel->IsConnected(0, ModelName::LG_MODEL_G27))
			return -2;
		return true;
	}

	DeviceState G27Lib::Update()
	{
		if (!IsConnected())
		{
			DeviceState empty;
			empty.IsInitialized = false;
			return empty;
		}
		_controllerInput->Update();
		_wheel->Update();

		return GetDeviceState();
	}

	DeviceState G27Lib::GetDeviceState()
	{
		DeviceState res;
		res.IsInitialized = true;

		DIJOYSTATE2 *state = _wheel->GetState(0);

		res.AcceleratorValue = state->lY;
		res.BrakeValue = state->lRz;
		res.ClutchValue = int(state->rglSlider[1]);
		res.WheelPosition = state->lX;
		res.ShifterPOV = int(state->rgdwPOV[0]);
		for (int i = 0; i < 128; i++)
			res.ButtonStates[i] = state->rgbButtons[i]==0?false:true;

		return res;

	}

	void G27Lib::PlayLEDS(int minRpm, int maxRpm, int currentRpm)
	{
		if (!IsConnected())
			return;

		_wheel->PlayLeds(0, currentRpm, minRpm, maxRpm);
	}

	void G27Lib::PlayBumpyRoadEffect(double magnitude)
	{		
		if (!IsConnected())
			return;
		_wheel->PlayBumpyRoadEffect(0, magnitude * 100);
	}

	void G27Lib::StopBumpyRoadEffect()
	{
		if (!IsConnected())
			return;
		_wheel->StopBumpyRoadEffect(0);
	}

	void G27Lib::PlayCarAirborne()
	{
		if (!IsConnected())
			return;
		_wheel->PlayCarAirborne(0);
	}

	void G27Lib::StopCarAirborne()
	{
		if (!IsConnected())
			return;
		_wheel->StopCarAirborne(0);
	}

	void G27Lib::PlayConstantForce(int magnitudePercentage)
	{
		if (!IsConnected())
			return;
		_wheel->PlayConstantForce(0, magnitudePercentage);
	}

	void G27Lib::StopConstantForce()
	{
		if (!IsConnected())
			return;
		_wheel->StopConstantForce(0);
	}

	void G27Lib::PlayDamperForce(int coefficientPercentage)
	{
		if (!IsConnected())
			return;
		_wheel->PlayDamperForce(0, coefficientPercentage);
	}

	void G27Lib::StopDamperForce()
	{
		if (!IsConnected())
			return;
		_wheel->StopDamperForce(0);
	}

	void G27Lib::PlayDirtRoadEffect(int magnitudePercentage)
	{
		if (!IsConnected())
			return;
		_wheel->PlayDirtRoadEffect(0, magnitudePercentage);
	}

	void G27Lib::StopDirtRoadEffect()
	{
		if (!IsConnected())
			return;
		_wheel->StopDirtRoadEffect(0);
	}

	void G27Lib::PlayFrontalCollisionForce(int magnitudePercentage)
	{
		if (!IsConnected())
			return;
		_wheel->PlayFrontalCollisionForce(0, magnitudePercentage);
	}

	void G27Lib::PlaySideCollisionForce(int magnitudePercentage)
	{
		if (!IsConnected())
			return;
		_wheel->PlaySideCollisionForce(0, magnitudePercentage);
	}

	void G27Lib::PlaySlipperyRoadEffect(int magnitudePercentage)
	{
		if (!IsConnected())
			return;
		_wheel->PlaySlipperyRoadEffect(0, magnitudePercentage);
	}

	void G27Lib::StopSlipperyRoadEffect()
	{
		if (!IsConnected())
			return;
		_wheel->StopSlipperyRoadEffect(0);
	}

	void G27Lib::PlaySoftstopForce(int usableRangePercentage)
	{
		if (!IsConnected())
			return;
		_wheel->PlaySoftstopForce(0, usableRangePercentage);
	}

	void G27Lib::StopSoftstopForce()
	{
		if (!IsConnected())
			return;
		_wheel->StopSoftstopForce(0);
	}

	void G27Lib::PlaySpringForce(int offsetPercentage, int saturationPercentage, int coefficientPercentage)
	{
		if (!IsConnected())
			return;
		_wheel->PlaySpringForce(0, offsetPercentage, saturationPercentage, coefficientPercentage);
	}

	void G27Lib::StopSpringForce()
	{
		if (!IsConnected())
			return;
		_wheel->StopSpringForce(0);
	}

	void G27Lib::PlaySurfaceEffect(int periodicType, int magnitude, int frequency)
	{
		if (!IsConnected())
			return;
		PeriodicType ptype;
		switch (periodicType)
		{
		case 2:
			ptype = PeriodicType::LG_TYPE_TRIANGLE;
			break;
		case 1:
			ptype = PeriodicType::LG_TYPE_SQUARE;
			break;
		case 0:
			ptype = PeriodicType::LG_TYPE_SINE;
			break;
		case -1:
		default:
			ptype = PeriodicType::LG_TYPE_NONE;
			break;
		}
		_wheel->PlaySurfaceEffect(0, ptype, magnitude, frequency);
	}

	void G27Lib::StopSurfaceEffect()
	{
		_wheel->StopSurfaceEffect(0);
	}
}